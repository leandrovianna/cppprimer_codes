/* ***********************************************************************
	* Filename: 1.2_exercise1.5.cpp
	* Date: 2014-02-05-11.18
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.5 - 1.2 A First Look at Input/Output
	* Page: 9
	* Description: Rewrite example 1.2 using a separate statement for
	print each output.
   *********************************************************************** */

#include <iostream>

int main()
{
    std::cout << "Enter two numbers:";
    std::cout << std::endl;
    int v1 = 0, v2 = 0;
    std::cin >> v1 >> v2;
    std::cout << "The sum of ";
    std::cout << v1;
    std::cout << " and ";
    std::cout << v2;
	std::cout << " is ";
	std::cout << v1 + v2;
	std::cout << std::endl;
	//too messy
    return 0;
}
