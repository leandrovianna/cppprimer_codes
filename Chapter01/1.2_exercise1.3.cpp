/* ***********************************************************************
	* Filename: 1.2_exercise1.3.cpp
	* Date: 2014-02-05-10.20
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.3 - 1.2 A First Look at Input/Output
	* Page: 9
	* Description: Prints "Hello, World" in Standart Output
   *********************************************************************** */

#include <iostream>

int main()
{
	std::cout << "Hello, World" << std::endl;
	return 0;
}
