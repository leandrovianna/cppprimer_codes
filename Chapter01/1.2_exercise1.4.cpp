/* ***********************************************************************
	* Filename: 1.2_exercise1.4.cpp
	* Date: 2014-02-05-11.06
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.4 - 1.2 A First Look at Input/Output
	* Page: 9
	* Description: Instead add two number, write a program that multiply
	two numbers. (based on example in 1.2)
   *********************************************************************** */

#include <iostream>

int main()
{
	std::cout << "Enter two numbers:" << std::endl;
	int v1 = 0, v2 = 0;
	std::cin >> v1 >> v2;
	std::cout << "The product of " << v1 << " and " << v2
				<< " is " << v1 * v2 << std::endl;
	return 0;
}
