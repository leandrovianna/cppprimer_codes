/* ***********************************************************************
	* Filename: 1.4.4_exercise1.19.cpp
	* Date: 2014-02-07-20.58
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.19 - 1.4.4 The if Statement
	* Page: 18
	* Description: Revised exercise 1.11 program. It handles input in which
	the first number is smaller than the second.
   *********************************************************************** */

#include <iostream>

int main()


    std::cout << "Enter two integers:" << std::endl;
    int n1, n2; //n1 is begin of range
				//n2 is end of range
    std::cin >> n1 >> n2;	//read two integers

    int val = n1; //val is the number that will printed

	if (n1 <= n2) {
		//	while is going to finish when val is greater than n2
		while (val <= n2) {
			std::cout << val << std::endl;	//print val
			++val;							//increment val
		}
	}
    return 0;
}
