/* ***********************************************************************
	* Filename: 1.3_exercise1.7.cpp
	* Date: 2014-02-05-11.58
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.7 - 1.3 A Word about Comments
	* Page: 11
	* Description: Compile a program that has incorrectly nested comments.
   *********************************************************************** */

/*
 * comment pairs /* */ cannot nest.
 * ‘‘cannot nest’’ is considered source code,
 * as is the rest of the program
 */
int main()
{
	return 0;
}
