/* ***********************************************************************
	* Filename: 1.4.3_exercise1.16.cpp
	* Date: 2014-02-07-11.25
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.16 - 1.4.3 Reading an Unknown Number of Inputs
	* Page: 17
	* Description: My own version of a program that prints the sum of a
	set of integers.
   *********************************************************************** */

#include <iostream>

int main()
{
    int num, sum = 0;
    // num stores each number read from cin.
    // sum stores the sum of the integers read.

    std::cout << "Enter a unknown number of integers:" << std::endl;

    //as long as the input is good, the loop continue.
    while (std::cin >> num)
        sum += num;		//add num to sum

    std::cout << "The sum of the set of integers is "
              << sum << std::endl;	//prints the sum of integers read
    return 0;
}
