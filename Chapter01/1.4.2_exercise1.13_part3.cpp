/* ***********************************************************************
	* Filename: 1.4.2_exercise1.13_part3.cpp
	* Date: 2014-02-06-15.51
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.13 - 1.4.2 The for Statement
	* Page: 14
	* Description: Rewrite exercise from 1.4.1 using for.
				   This is exercise 1.11
   *********************************************************************** */

#include <iostream>

int main()
{
    std::cout << "Enter two integers:" << std::endl;
    int n1, n2; //n1 is begin of range
				//n2 is end of range
    std::cin >> n1 >> n2;	//read two integers

    for (int val = n1; val <= n2; ++val)
		std::cout << val << std::endl;	//print val
    return 0;
}
