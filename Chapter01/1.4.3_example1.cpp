/* ***********************************************************************
	* Filename: 1.4.3_example1.cpp
	* Date: 2014-02-06-19.13
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.4.3 Reading a Unknown Number of Inputs
	* Page: 15
	* Description: Sum of a unknown numbers of values
   *********************************************************************** */

#include <iostream>
int main()
{
    int sum = 0, value = 0;
	// read until end-of-file, calculating a running total of all values read
    while (std::cin >> value)
        sum += value; // equivalent to sum = sum + value
    std::cout << "Sum is: " << sum << std::endl;
    return 0;
}
