/* ***********************************************************************
	* Filename: 1.5.2_exercise1.23.cpp
	* Date: 2014-02-10-15.17
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.23 - 1.5.2 A First Look At Member Functions
	* Page: 24
	* Description: Reads several transactions and counts how many
	transactions occur for each ISBN.
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item item, curr_item;

    if (std::cin >> curr_item) {
        int count = 1;

        while (std::cin >> item) {
            if (item.isbn() == curr_item.isbn()) {
                ++count;
            } else {
                std::cout << curr_item.isbn() << " occurs "
                          << count << " times."
                          << std::endl;
                count = 1;
                curr_item = item;
            }
        }

        std::cout << curr_item.isbn() << " occurs "
                          << count << " times."
                          << std::endl;
    }
    return 0;
}
