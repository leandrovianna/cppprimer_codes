/* ***********************************************************************
	* Filename: 1.5.1_exercise1.21.cpp
	* Date: 2014-02-09-16.50
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.21 - 1.5.1 The Sales_item class
	* Page: 22
	* Description: Reads two Sales_item objects that have the same ISBN
	and produces their sum.
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item item1, item2;

	// prompts the user
    std::cout << "Enter two book transactions:" << std::endl;

	// read transactions (must have the same ISBN)
    std::cin >> item1 >> item2;

    // test if item1's ISBN is equal to item2's ISBN
	if (item1.isbn() == item2.isbn()) {
		std::cout << "The sum of two transactions is "
					<< item1 + item2 << std::endl;
	}
	else {
		// ISBN is not equal
		std::cerr << "The ISBN is different in these transactions."
					<< std::endl; //error message
	}
    return 0;
}
