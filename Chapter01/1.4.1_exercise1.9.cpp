/* ***********************************************************************
	* Filename: 1.4.1_exercise1.9.cpp
	* Date: 2014-02-06-09.42
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.9 - 1.4.1 The while Statement
	* Page: 13
	* Description: Uses a while to sum the numbers from 50 to 100
   *********************************************************************** */

#include <iostream>

int main()
{
	int sum = 0, val = 50;

	while (val <= 100) {
		sum += val;		//assign sum + val to sum
		++val;			//increment val (val = val + 1)
	}
	std::cout << "Sum of numbers from 50 to 100 is "
				<< sum << std::endl;
	return 0;
}
