/* ***********************************************************************
	* Filename: 1.4.2_exercise1.13_part2.cpp
	* Date: 2014-02-06-15.49
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.13 - 1.4.2 The for Statement
	* Page: 14
	* Description: Rewrite exercise from 1.4.1 using for.
				   This is exercise 1.10
   *********************************************************************** */

#include <iostream>

int main()
{
	for (int num = 10; num >= 0; --num)
		std::cout << num << std::endl;		//prints the number
	return 0;
}
