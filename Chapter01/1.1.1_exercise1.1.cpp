/* ***********************************************************************
	* Filename: 1.1.1_exercise1.1.cpp
	* Date: 2014-02-05-10.15
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.1 - 1.1.1 Compiling and Executing Our Program
	* Page: 5
	* Description: Compile and Run this program
   *********************************************************************** */

int main()
{
    return 0;
}
