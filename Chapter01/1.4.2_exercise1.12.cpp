/* ***********************************************************************
	* Filename: 1.4.2_exercise1.12.cpp
	* Date: 2014-02-06-15.36
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.12 - 1.4.2 The for Statement
	* Page: 14
	* Description: A answer for a question (conceptual)
   *********************************************************************** */

/* Question: What does the following for loop do? What is the final value of sum?
	int sum = 0;
	for (int i = -100; i <= 100; ++i)
		sum += i;


Answer: Ele inicia i com -100 e testa a condição se i é menor ou igual a 100.
		Ao final de cada execução do bloco é acrescentado 1 a i e é testada a
		condição. No bloco ele soma i a variavel sum e depois atribui esse
		resultado a sum.

		Ao final o valor de sum deve ser 0.
*/

#include <iostream>
int main()
{
	int sum = 0;
	for (int i = -100; i <= 100; ++i)
		sum += i;

	std::cout << "Final value of sum is " << sum << std::endl;
	return 0;
}
