/* ***********************************************************************
	* Filename: 1.4.1_exercise1.10.cpp
	* Date: 2014-02-06-11.23
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.10 - 1.4.1 The while Statement
	* Page: 13
	* Description: Using the decrement operator (--) to write a while that
	prints the numbers from ten down to zero.
   *********************************************************************** */

#include <iostream>

int main()
{
	int num = 10;	//num starts with value ten

	//	while keep executing as long as num is greater than or equal 0
	while (num >= 0 ) {
		std::cout << num << std::endl;	//prints the number
		--num;							//decrement the number (10..9..8....0)
	}
	return 0;
}
