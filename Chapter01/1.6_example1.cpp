/* ***********************************************************************
	* Filename: 1.6_example1.cpp
	* Date: 2014-02-16-10.32
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.6 The Bookstore Program
	* Page: 24-25
	* Description: The bookstore program. Read a sequence of sales transactions,
	and produce a report with transactions totals for each ISBN.
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item total;	//variable to hold data for the next transactions

    // read the first transaction and ensure that there are data to process
    if (std::cin >> total) {
		Sales_item trans;	//variable to hold the running sum
		//	read and process the remaining transactions
		while (std::cin >> trans) {
			// if we're still process the same book
			if (total.isbn() == trans.isbn())
				total += trans; //	update the running total
			else {
                //	print the result for the previous book
                std::cout << total << std::endl;
                total = trans;	//	total now refers to the next book
			}
		}
		std::cout << total << std::endl; //	print the last transactions
    } else {
		//	no input! warn the user
		std::cerr << "No data?!" << std::endl;
		return -1;	//	indicate failure
    }
    return 0;
}
