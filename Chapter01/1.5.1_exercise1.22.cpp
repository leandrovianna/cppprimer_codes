/* ***********************************************************************
	* Filename: 1.5.1_exercise1.22.cpp
	* Date: 2014-02-09-16.50
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.22 - 1.5.1 The Sales_item class
	* Page: 22
	* Description: Reads several transactions for the same ISBN.
	Write the sum of all the transactions that were read.
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
	Sales_item item, sum;

	if (std::cin >> sum) {
		while (std::cin >> item) {
			if (item.isbn() == sum.isbn()) {
				//tests if items have the same isbn
				sum += item; //if yes, increment sum with new item
			}
			else {
				//otherwise, print the previous transactions
				//and assign to sum the new transaction
				std::cout << "The sum of this sequence of transactions is "
							<< sum << std::endl;

				//reset variables
				sum = item;
			}
		}

		std::cout << "The sum of this sequence of transactions is "
					<< sum << std::endl;
	}
	return 0;
}
