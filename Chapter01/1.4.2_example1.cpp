/* ***********************************************************************
	* Filename: 1.4.2_example1.cpp
	* Date: 2014-02-06-15.23
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.4.2 The for Statement
	* Page: 13
	* Description: Sum of 1 to 10 inclusive (using for).
   *********************************************************************** */

#include <iostream>

int main()
{
	int sum = 0;
	//	sum values from 1 through 10 inclusive
	for (int val = 1; val <= 10; ++val)
		sum += val;		// equivalent to sum = sum + val
	std::cout << "Sum of 1 to 10 inclusive is "
				<< sum << std::endl;
	return 0;
}
