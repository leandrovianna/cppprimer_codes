/* ***********************************************************************
	* Filename: 1.5.1_exercise1.20.cpp
	* Date: 2014-02-09-16.30
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.20 - 1.5.1 The Sales_item class
	* Page: 22
	* Description: A program that reads a set of book sales transactions,
	writing each transaction to the standard output (using Sales_item
	class).
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item transaction;

	// prompts the user
	std::cout << "Enter a set of book sales transactions:" << std::endl;

    // reads each transaction that the user enter
    while (std::cin >> transaction) {
		std::cout << transaction << std::endl;
    }
    return 0;
}
