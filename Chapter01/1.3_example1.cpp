/* ***********************************************************************
	* Filename: 1.3_example1.cpp
	* Date: 2014-02-05-11.51
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.3 A Word About Comments
	* Page: 10
	* Description: Comment a code
   *********************************************************************** */

#include <iostream>
/*
 * Simple main function:
 * Read two numbers and write their sum
 */

int main()
{
	// prompts the user to enter two numbers
    std::cout << "Enter two numbers:" << std::endl;
    int v1 = 0, v2 = 0;		//	variables to hold the input we read
    std::cin >> v1 >> v2;	//	read input
    std::cout << "The sum of " << v1 << " and " << v2
              << " is " << v1 + v2 << std::endl;
    return 0;
}
