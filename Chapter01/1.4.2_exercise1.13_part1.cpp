/* ***********************************************************************
	* Filename: 1.4.2_exercise1.13_part1.cpp
	* Date: 2014-02-06-15.46
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.13 - 1.4.2 The for Statement
	* Page: 14
	* Description: Rewrite exercise from 1.4.1 using for.
				   This is exercise 1.9
   *********************************************************************** */
#include <iostream>

int main()
{
	int sum = 0;

	for (int val = 50; val <= 100; ++val)
		sum += val;		//assign sum + val to sum
	std::cout << "Sum of numbers from 50 to 100 is "
				<< sum << std::endl;
	return 0;
}
