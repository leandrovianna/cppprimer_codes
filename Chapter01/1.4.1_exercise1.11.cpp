/* ***********************************************************************
	* Filename: 1.4.1_exercise1.11.cpp
	* Date: 2014-02-06-15.04
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Exercise 1.11 - 1.4.1 The while Statement
	* Page: 13
	* Description: Print numbers in the range specified by user by two
	integers.
   *********************************************************************** */

#include <iostream>

int main()
{
    std::cout << "Enter two integers:" << std::endl;
    int n1, n2; //n1 is begin of range
				//n2 is end of range
    std::cin >> n1 >> n2;	//read two integers

    int val = n1; //val is the number that will printed

    //	while is going to finish when val is greater than n2
    while (val <= n2) {
		std::cout << val << std::endl;	//print val
		++val;							//increment val
    }
    return 0;
}
