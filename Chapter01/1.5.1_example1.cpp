/* ***********************************************************************
	* Filename: 1.5.1_example1.cpp
	* Date: 2014-02-09-15.26
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.5.1 The Sales_item class
	* Page: 21
	* Description: Reading and Writing a Sales_item
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item book;

    // read ISBN, number of copies sold, and sales price
    std::cin >> book;

    // write ISBN, number of copies sold, total revenue, and average price
    std::cout << book;

    return 0;
}
