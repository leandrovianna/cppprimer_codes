/* ***********************************************************************
	* Filename: 1.4.1_example1.cpp
	* Date: 2014-02-05-15.06
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.4.1 The while Statement
	* Page: 11
	* Description: Sum 1 to 10 using while
   *********************************************************************** */

#include <iostream>

int main()
{
	int sum = 0, val = 1;
	//	keep executing the while as long as val is less than or equal to 10
	while ( val <= 10) {
		sum += val;		// assigns sum + val to sum
		++val;			// add 1 to val
	}
	std::cout << "Sum of 1 to 10 inclusive is "
			<< sum << std::endl;
	return 0;
}
