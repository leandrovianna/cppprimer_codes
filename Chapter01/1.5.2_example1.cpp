/* ***********************************************************************
	* Filename: 1.5.2_example1.cpp
	* Date: 2014-02-09-22.52
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.5.2 A First Look at Member Functions
	* Page: 23
	* Description: Add two Sales_item improved.
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
	Sales_item item1, item2;

	std::cin >> item1 >> item2;

	//	first check that item1 and item2 represent the same book
	if (item1.isbn() == item2.isbn()) {
		std::cout << item1 + item2 << std::endl;
		return 0;	//	indicate success
	} else {
		std::cerr << "Data must refer to same ISBN"
					<< std::endl;
		return -1;	//	indicate failure
	}
}
