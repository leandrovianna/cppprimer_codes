/* ***********************************************************************
	* Filename: 1.5.1_example2.cpp
	* Date: 2014-02-09-15.33
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 1: Getting Started
	* Example 1 - 1.5.1 The Sales_item class
	* Page: 21
	* Description: Adding two Sales_item
   *********************************************************************** */

#include <iostream>
#include "Sales_item.h"

int main()
{
	Sales_item item1, item2;
	std:: cin >> item1 >> item2;				// read a pair of transactions
	std::cout << item1 + item2 << std::endl;	// print their sum
	return 0;
}
