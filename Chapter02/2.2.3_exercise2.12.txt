Exercise 2.12
Date: 2014-03-02-13.11

Question:
Which, if any, of the following names are invalid?
(a) int double = 3.14;
(b) int _;
(c) int catch-22;
(d) int 1_or_2 = 1;
(e) double Double = 3.14;

Answer:
(a), (b), (c) and (d) are invalid!
