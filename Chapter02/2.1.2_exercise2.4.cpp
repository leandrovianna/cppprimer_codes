/* ***********************************************************************
	* Filename: 2.1.2_exercise2.4.cpp
	* Date: 2014-02-23-13.00
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 2: Variables and Basics types
	* Exercise 2.4 - 2.1.2 Type conversions
	* Page: 38
	* Description: Check my predictions (exercise 2.3)  were correct.
   *********************************************************************** */

#include <iostream>

int main()
{
    //code from exercise 2.3
    unsigned u = 10, u2 = 42;
    std::cout << u2 - u << std::endl;
    std::cout << u - u2 << std::endl;
    int i = 10, i2 = 42;
    std::cout << i2 - i << std::endl;
    std::cout << i - i2 << std::endl;
    std::cout << i - u << std::endl;
    std::cout << u - i << std::endl;

    return 0;

    //Conclusion: The predictions were correct.
    /* Output in my computer
    32
    4294967264
    32
    -32
    0
    0
    */
}
