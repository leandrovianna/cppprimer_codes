Exercise 2.16
Date: 2014-03-04-19.15

Question:
Which, if any, of the following assignments are invalid? If they are
valid, explain what they do.

int i = 0, &r1 = i; double d = 0, &r2 = d;
(a) r2 = 3.14159;
(b) r2 = r1;
(c) i = r2;
(d) r1 = d;

Answer:
All assignments are valid.
(a): assign to d the value 3.14159
(b): assign to d (r2 refers to d) the value of i (r1 refers to i)
(c): assign to i the value of d (r2 refers to d)
(d): assign to i (r1 refers to i) the value of d
