/* ***********************************************************************
	* Filename: 2.2.4_example1.cpp
	* Date: 2014-03-02-22.07
	* Author (Programmer): Leandro Vianna <leandrovianna50@gmail.com>
	* C++ Primer - Stanley B. Lippman, Josée Lajoie and Barbara E. Moo
	* Chapter 2: Variables and Basic Types
	* Example 1 - 2.2.4 Scope of a name
	* Page: 49
	* Description: Show examples of scope
   *********************************************************************** */

#include <iostream>
// Program for illustration purposes only: It is bad style for a function
// to use a global variable and also define a local variable with the same name
int reused = 42; // reused has global scope

int main()
{
    int unique = 0; // unique has block scope
	// output #1: uses global reused; prints 42 0
    std::cout << reused << " " << unique << std::endl;
    int reused = 0; // new, local object named reused hides global reused
	// output #2: uses local reused; prints 0 0
    std::cout << reused << " " << unique << std::endl;
	// output #3: explicitly requests the global reused; prints 42 0
    std::cout << ::reused << " " << unique << std::endl;
    return 0;
}
